import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/customers/entities/customer.entity';
import { Product } from 'src/products/entities/product.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';
import { Table } from 'src/table/entities/table.entity';
import { ProductQueue } from 'src/product-queue/entities/product-queue.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
    @InjectRepository(Table)
    private tableRepository: Repository<Table>,
    @InjectRepository(ProductQueue)
    private productQueue: Repository<ProductQueue>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    // console.log(createOrderDto.tableId);
    const customer = await this.customersRepository.findOneBy({
      id: createOrderDto.customerId,
    });
    // const table = await this.tableRepository.findOneBy({
    //   id: createOrderDto.table.id,
    // });
    const order: Order = new Order();
    order.customer = customer;
    order.amount = 0;
    order.total = 0;
    order.status = createOrderDto.status;
    order.table = createOrderDto.table;
    console.log(createOrderDto.table);
    // order.table = createOrderDto.
    await this.ordersRepository.save(order); // ได้ id

    for (const od of createOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.amount = od.amount;
      orderItem.product = await this.productsRepository.findOneBy({
        id: od.productId,
      });
      orderItem.name = orderItem.product.name;
      orderItem.price = orderItem.product.price;
      orderItem.total = orderItem.price * orderItem.amount;
      orderItem.order = order; // อ้างกลับ
      orderItem.level = od.level;
      await this.orderItemsRepository.save(orderItem);
      order.amount = order.amount + orderItem.amount;
      order.total = order.total + orderItem.total;
    }
    await this.ordersRepository.save(order); // ได้ id
    return await this.ordersRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems', 'table'],
    });
  }

  findAll() {
    return this.ordersRepository.find({
      relations: ['customer', 'orderItems', 'table'],
    });
  }

  findOne(id: number) {
    return this.ordersRepository.findOne({
      where: { id: id },
      relations: ['customer', 'orderItems'],
    });
  }

  async update(id: number, updateOrderDto: UpdateOrderDto) {
    //ดึง order ออกมาจากฐานข้อมูล
    const order: Order = await this.ordersRepository.findOneBy({ id });

    // วนลูปเพื่อสร้าง order item และ food queue ใหม่จาก updateOrderDto

    for (const od of updateOrderDto.orderItems) {
      // สร้าง order item ใหม่
      const orderItem = new OrderItem();
      orderItem.order = order;
      orderItem.amount = od.amount;
      orderItem.note = od.note;
      // orderItem.order.id = id;
      orderItem.product = od.product;
      orderItem.name = od.name;
      orderItem.price = od.price;
      orderItem.total = od.amount * od.price;
      orderItem.level = od.level;
      // orderItem.status = od.status;
      // const table = await this.tablesRepository.findOneBy({ id: 1 });
      orderItem.table = od.table;
      await this.orderItemsRepository.save(orderItem);

      order.amount = order.amount + orderItem.amount;
      order.total = order.total + orderItem.total;
      order.status = updateOrderDto.status;


      // แบบ Save ตามจำนวนที่สั่ง
      // for (let index = 0; index < od.amount; index++) {
      //   const productQueue: ProductQueue = new ProductQueue();
      //   productQueue.name = od.name;
      //   productQueue.note = od.note;
      //   productQueue.price = od.product.price;
      //   productQueue.orderItem = orderItem;
      //   productQueue.status = 'รอทำ';

      //   // บันทึก order item เดิมและ food queue ใหม่ลงในฐานข้อมูล
      //   productQueue.orderItem = await this.orderItemsRepository.save(
      //     orderItem,
      //   );
      //   await this.productQueue.save(productQueue);
      // }


      // แบบ Save รอบเดียว
      const productQueue: ProductQueue = new ProductQueue();
      productQueue.name = od.name;
      productQueue.note = od.note;
      productQueue.price = od.product.price;
      productQueue.orderItem = orderItem;
      productQueue.status = 'รอทำ';

      // บันทึก order item เดิมและ food queue ใหม่ลงในฐานข้อมูล
      productQueue.orderItem = await this.orderItemsRepository.save(
        orderItem,
      );
      await this.productQueue.save(productQueue);
    }

    // บันทึก order
    await this.ordersRepository.update(id, order);
    console.log(id);
    return 'Success';
  }

  async remove(id: number) {
    const order = await this.ordersRepository.findOneBy({ id: id });
    return this.ordersRepository.softRemove(order);
  }

  async getBillByTable(id: number) {
    const order = await this.productQueue
      .createQueryBuilder('productQueue')
      .leftJoinAndSelect('productQueue.orderItem', 'orderItem')
      .leftJoinAndSelect('orderItem.table', 'table')
      .leftJoinAndSelect('orderItem.order', 'order')
      .where('table.id = :id AND productQueue.status = "เสิร์ฟแล้ว"', {
        id: id,
      })
      .getMany();
    const table = await this.tableRepository.findOne({
      where: { id: id },
    });

    return {
      table: table,
      order_list: order.map((e, i) => e.orderItem),
      order: order.length > 0 ? order[0].orderItem.order : null,
    };
  }

  async checkOutBill(id: number) {
    const order = await this.ordersRepository.findOne({ where: { id: id } });
    const order_items = await this.orderItemsRepository.find({
      where: {
        order: order,
      },
      relations: ['table'],
    });

    if (order_items.length > 0) {
      console.log(order_items);
      await this.tableRepository.update(order_items[0].table.id, {
        status: 'ว่าง',
      });

      await this.productQueue
        .createQueryBuilder('productQueue')
        .update()
        .set({ status: 'ชำระเงินแล้ว' })
        .where('orderItemId IN (:...id)', { id: order_items.map((e) => e.id) })
        .execute();

      await this.ordersRepository.update(id, { status: 'ชำระเงินแล้ว' });
    }

    return 'Success';
  }
}
