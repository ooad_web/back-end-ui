import { Product } from 'src/products/entities/product.entity';
import { Table } from 'src/table/entities/table.entity';
import { OrderItem } from '../entities/order-item';

class CreatedOrderItemDto {
  productId: number;
  amount: number;
  product: Product;
  name: string;
  note: string;
  price: number;
  table: Table;
  level: string;
  // orderItems: OrderItem[];
}
export class CreateOrderDto {
  customerId: number;
  orderItems: CreatedOrderItemDto[];
  table: Table;
  userId: number;
  status: string;
  // tableId: number;
}
