import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomersModule } from './customers/customers.module';
import { Customer } from './customers/entities/customer.entity';
import { Product } from './products/entities/product.entity';
import { ProductsModule } from './products/products.module';
import { OrdersModule } from './orders/orders.module';
import { Order } from './orders/entities/order.entity';
import { OrderItem } from './orders/entities/order-item';
import { UsersModule } from './users/users.module';
import { User } from './users/entities/user.entity';
import { AuthModule } from './auth/auth.module';
import { TableModule } from './table/table.module';
import { Table } from './table/entities/table.entity';
import { ProductQueueModule } from './product-queue/product-queue.module';
import { ProductQueue } from './product-queue/entities/product-queue.entity';
import { StocksModule } from './stocks/stocks.module';
import { Stock } from './stocks/entities/stock.entity';
import { Employee } from './employees/entities/employee.entity';
import { EmployeesModule } from './employees/employees.module';
import { CheckinoutModule } from './checkinout/checkinout.module';
import { Checkinout } from './checkinout/entities/checkinout.entity';
import { SalarysModule } from './salarys/salarys.module';
import { Salary } from './salarys/entities/salary.entity';
import { PaymentModule } from './payment/payment.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(
      //   {
      //   type: 'sqlite',
      //   database: 'db.sqlite',
      //   synchronize: true,
      //   migrations: [],
      //   entities: [Customer, Product, Order, OrderItem, User],
      // }
      {
        type: 'mysql',
        host: 'hellodb.c7uglhum6eri.ap-southeast-1.rds.amazonaws.com',
        port: 3306,
        username: 'dbproject',
        password: 'pass1234',
        database: 'dbproject',
        entities: [
          Customer,
          Product,
          Order,
          OrderItem,
          User,
          Table,
          ProductQueue,
          Employee,
          Stock,
          Checkinout,
          Salary,
        ],
        synchronize: true,
      },
    ),
    CustomersModule,
    EmployeesModule,
    CheckinoutModule,
    ProductsModule,
    OrdersModule,
    UsersModule,
    AuthModule,
    TableModule,
    ProductQueueModule,
    StocksModule,
    SalarysModule,
    PaymentModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
