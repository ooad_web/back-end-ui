import { IsNotEmpty } from 'class-validator';

export class CreateTableDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  amount: number;
  @IsNotEmpty()
  status: string;
}
