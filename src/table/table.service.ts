import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTableDto } from './dto/create-table.dto';
import { UpdateTableDto } from './dto/update-table.dto';
import { Table } from './entities/table.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class TableService {
  constructor(
    @InjectRepository(Table)
    private tableRepository: Repository<Table>,
  ) {}
  create(createTableDto: CreateTableDto) {
    return this.tableRepository.save(createTableDto);
  }

  findAll() {
    return this.tableRepository.find();
  }

  findOne(id: number) {
    return this.tableRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateTableDto: UpdateTableDto) {
    try {
      const updatedTable = await this.tableRepository.save({
        id,
        ...updateTableDto,
      });
      return updatedTable;
    } catch (e) {
      throw new NotFoundException();
    }
  }
  async remove(id: number) {
    const table = await this.tableRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedTable = await this.tableRepository.remove(table);
      return deletedTable;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
