import { Module } from '@nestjs/common';
import { TableService } from './table.service';
import { TableController } from './table.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Table } from './entities/table.entity';
import { Order } from 'src/orders/entities/order.entity';
import { OrderItem } from 'src/orders/entities/order-item';

@Module({
  imports: [TypeOrmModule.forFeature([Table, Order, OrderItem])],
  controllers: [TableController],
  providers: [TableService],
})
export class TableModule {}
