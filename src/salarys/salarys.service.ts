import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Checkinout } from 'src/checkinout/entities/checkinout.entity';
import { Repository } from 'typeorm';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
import { Salary } from './entities/salary.entity';

@Injectable()
export class SalarysService {
  constructor(
    @InjectRepository(Salary)
    private salarysRepository: Repository<Salary>,
    @InjectRepository(Checkinout)
    private checkInOutRepository: Repository<Checkinout>,
  ) {}

  create(createSalaryDto: CreateSalaryDto) {
    return this.salarysRepository.save(createSalaryDto);
  }
  findAllSalary() {
    return this.checkInOutRepository
      .createQueryBuilder('checkinout')
      .leftJoinAndSelect('checkinout.employee', 'employee')
      .getMany();
  }
  findSalaryByName(name: string) {
    return this.checkInOutRepository
      .createQueryBuilder('checkinout')
      .leftJoinAndSelect('checkinout.employee', 'employee')
      .where('employee.firstname = :firstname', { firstname: name })
      .getMany();
  }
  findAll() {
    return this.salarysRepository.find({ relations: ['employee'] });
  }

  findOne(id: number) {
    return this.salarysRepository.findOne({ where: { id } });
  }

  async update(id: number, updateSalaryDto: UpdateSalaryDto) {
    const employee = await this.salarysRepository.findOneBy({ id: id });
    if (!employee) {
      throw new NotFoundException();
    }
    const updateSalary = { ...employee, ...updateSalaryDto };
    return this.salarysRepository.save(updateSalary);
  }

  async remove(id: number) {
    const salary = await this.salarysRepository.findOneBy({ id: id });
    if (!salary) {
      throw new NotFoundException();
    }
    return this.salarysRepository.remove(salary);
  }
}
