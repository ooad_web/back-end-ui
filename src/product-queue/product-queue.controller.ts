import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ProductQueueService } from './product-queue.service';
import { CreateProductQueueDto } from './dto/create-product-queue.dto';
import { UpdateProductQueueDto } from './dto/update-product-queue.dto';

@Controller('product-queue')
export class ProductQueueController {
  constructor(private readonly productQueueService: ProductQueueService) {}

  @Post()
  create(@Body() createProductQueueDto: CreateProductQueueDto) {
    return this.productQueueService.create(createProductQueueDto);
  }

  @Get()
  findAll() {
    return this.productQueueService.findAll();
  }

  @Get('/status/:status')
  findAllStatus(@Param('status') status: string) {
    return this.productQueueService.findAllStatus(status);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.productQueueService.findOne(+id);
  }

  @Patch('/status/')
  changeStatus(@Body() payload: { ids: number[]; status: string }) {
    const status = payload.status;
    const ids = payload.ids;

    return this.productQueueService.changeStatus(ids, status);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateProductQueueDto: UpdateProductQueueDto,
  ) {
    return this.productQueueService.update(+id, updateProductQueueDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.productQueueService.remove(+id);
  }
}
