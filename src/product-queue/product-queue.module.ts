import { Module } from '@nestjs/common';
import { ProductQueueService } from './product-queue.service';
import { ProductQueueController } from './product-queue.controller';
import { ProductQueue } from './entities/product-queue.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderItem } from 'src/orders/entities/order-item';

@Module({
  imports: [TypeOrmModule.forFeature([ProductQueue, OrderItem])],
  controllers: [ProductQueueController],
  providers: [ProductQueueService],
})
export class ProductQueueModule {}
