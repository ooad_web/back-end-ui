import { PartialType } from '@nestjs/mapped-types';
import { CreateProductQueueDto } from './create-product-queue.dto';

export class UpdateProductQueueDto extends PartialType(CreateProductQueueDto) {}
