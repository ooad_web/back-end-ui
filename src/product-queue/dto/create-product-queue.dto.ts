import { OrderItem } from 'src/orders/entities/order-item';

export class CreateProductQueueDto {
  orderItem: OrderItem;
  name: string;
  note: string;
  price: number;
  status: string;
}
