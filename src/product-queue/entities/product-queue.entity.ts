import { OrderItem } from 'src/orders/entities/order-item';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ProductQueue {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  note: string;

  @Column()
  status: string;

  @ManyToOne(() => OrderItem, (ordertem) => ordertem.productQueue)
  orderItem: OrderItem;
}
