import { Module } from '@nestjs/common';
import { EmployeesService } from './employees.service';
import { EmployeesController } from './employees.controller';
import { Employee } from './entities/employee.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Checkinout } from 'src/checkinout/entities/checkinout.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Employee, Checkinout])],
  controllers: [EmployeesController],
  providers: [EmployeesService],
})
export class EmployeesModule {}
