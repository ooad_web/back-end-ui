import { Transform } from 'class-transformer';
import { TransformFnParams } from 'class-transformer/types/interfaces';
import { Checkinout } from 'src/checkinout/entities/checkinout.entity';
import { Order } from 'src/orders/entities/order.entity';
// import { Salary } from 'src/salary/entities/salary.entity';
import { User } from 'src/users/entities/user.entity';

import {
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  firstname: string;

  @Column({
    length: '32',
  })
  lastname: string;

  @Column()
  tel: string;

  @Column()
  email: string;

  @Column()
  position: string;

  @Column()
  time_inout: string;

  @Column()
  date: string;

  // @OneToMany(() => Salary, (salary) => salary.employee)
  // salary: Salary[];

  @OneToMany(() => Checkinout, (checkinout) => checkinout.employee)
  checkinout: Checkinout[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deleteddAt: Date;
  salary: any;
}
// function moment(date1: TransformFnParams) {
//   throw new Error('Function not implemented.');
// }
