import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCheckinoutDto } from './dto/create-checkinout.dto';
import { UpdateCheckinoutDto } from './dto/update-checkinout.dto';
import { Checkinout } from './entities/Checkinout.entity';

@Injectable()
export class CheckinoutService {
  constructor(
    @InjectRepository(Checkinout)
    private ciosRepository: Repository<Checkinout>,
  ) {}

  create(createCheckinoutDto: CreateCheckinoutDto) {
    return this.ciosRepository.save(createCheckinoutDto);
  }

  // findAll() {
  //   return this.ciosRepository
  //     .createQueryBuilder('checkinout')
  //     .leftJoinAndSelect('checkinout.employee', 'employee')
  //     .where('checkinout.hour = :hour', { hour: 0 })
  //     .getMany();
  // }
  findAll() {
    return this.ciosRepository.find({ relations: ['employee'] });
  }

  findAllForPay() {
    return this.ciosRepository
      .createQueryBuilder('checkinout')
      .select('employee.id', 'employee_id')
      .addSelect('SUM(checkinout.hour)', 'total_hours')
      .leftJoin('checkinout.employee', 'employee')
      .where('checkinout.status = :status', { status: 'ยังไม่ชำระ' })
      .groupBy('employeeId')
      .addSelect('employee')
      .getRawMany();
  }

  findOne(id: number) {
    return this.ciosRepository.findOne({ where: { id } });
  }

  async update(id: number, updateCheckinoutDto: UpdateCheckinoutDto) {
    const checkinout = await this.ciosRepository.findOneBy({ id: id });
    if (!checkinout) {
      throw new NotFoundException();
    }
    const updateEmployee = { ...checkinout, ...updateCheckinoutDto };
    return this.ciosRepository.save(updateEmployee);
  }

  async remove(id: number) {
    const checkinout = await this.ciosRepository.findOneBy({ id: id });
    if (!checkinout) {
      throw new NotFoundException();
    }
    return this.ciosRepository.softRemove(checkinout);
  }
}
