import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { Stock } from './entities/stock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class StocksService {
  constructor(
    @InjectRepository(Stock)
    private stocksRepository: Repository<Stock>,
  ) {}

  create(createStockDto: CreateStockDto) {
    return this.stocksRepository.save(createStockDto);
  }

  findAll() {
    return this.stocksRepository.find();
  }

  async findOne(id: number) {
    const stock = await this.stocksRepository.findOne({
      where: { id: id },
    });
    if (!stock) {
      throw new NotFoundException();
    }
    return stock;
  }

  async update(id: number, updateStockDto: UpdateStockDto) {
    const stock = await this.stocksRepository.findOneBy({ id: id });
    if (!stock) {
      throw new NotFoundException();
    }
    const updateStock = { ...stock, ...updateStockDto };
    return this.stocksRepository.save(updateStock);
  }

  async remove(id: number) {
    const stock = await this.stocksRepository.findOneBy({ id: id });
    if (!stock) {
      throw new NotFoundException();
    }
    return this.stocksRepository.remove(stock);
  }
}
